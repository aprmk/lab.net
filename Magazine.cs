﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace laba.NET
{
    [Serializable]
    public class Magazine : Edition, IRateAndCopy
    {
        private Frequency _periodicity;
        private Article[] _list;
        private ArrayList _redactorsList;
        private ArrayList _articleList;
        
        
        private System.Collections.Generic.List<Person> _redactors;
        private System.Collections.Generic.List<Article> _articles;

        public bool this[Frequency index]
        {
            get
            {
                if (index == _periodicity)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public ArrayList this[double index]
        {
            get
            {
                ArrayList result = new ArrayList();
                foreach (var article in _list)
                {
                    if (article.Rating >= index)
                    {
                        result.Add(article);
                    }
                }

                return result;
            }
        }

        public ArrayList this[string index]
        {
            get
            {
                ArrayList result = new ArrayList();
                foreach (var article in _list)
                {
                    if (article.Name.Contains(index))
                    {
                        result.Add(article);
                    }
                }

                return result;
            }
        }

        public Magazine ShallowCopy()
        {
            return (Magazine)this.MemberwiseClone();
        }
        
        public override  object DeepCopy()
        {
            Magazine m = new Magazine(Name, Periodicity, DateRelease, CountPublication, new ArrayList(),
                new ArrayList());
            m.List = new Article[List.Length];
            for (int i = 0; i < List.Length; i++)
            {
                m.List[i] = (Article) List[i].DeepCopy();
            }

            for (int i = 0; i < _articleList.Count; i++)
            {
                m._articleList.Add((Article) (_articleList[i] as Article).DeepCopy());
            }

            for (int i = 0; i < _redactorsList.Count; i++)
            {
                m._redactorsList.Add((Person) (_redactorsList[i] as Person).DeepCopy());
            }

            return m;
        }


        public Magazine(string name, Frequency periodicity, DateTime dateRelease, int countPublication,
            ArrayList redactorsList, ArrayList articleList) : base(name, dateRelease, countPublication)
        {
            Name = name;
            Periodicity = periodicity;
            CountPublication = countPublication;
            List = new Article[] { };
            RedactorsList = redactorsList;
            _articleList = articleList;
        }

        public Magazine() : this("StudLife", Frequency.Montly, DateTime.Now, 3, new ArrayList(), new ArrayList())

        {
        }
        

        public Frequency Periodicity
        {
            get { return _periodicity; }
            set { _periodicity = value; }
        }

        public override DateTime DateRelease
        {
            get { return _dateRelease; }
            set { _dateRelease = value; }
        }

        public override int CountPublication
        {
            get { return _countPublication; }
            set { _countPublication = value; }
        }

        public Article[] List
        {
            get { return _list; }
            set { _list = value; }
        }

        public ArrayList RedactorsList
        {
            get { return _redactorsList; }
            set { _redactorsList = value; }
        }

        /// <summary>
        ///властивість з методами get, set
        /// LABA 2
        /// </summary>
        public Edition obj
        {
            get { return this; }
            set
            {
                _name = value.Name;
                _dateRelease = value.DateRelease;
                _countPublication = value.CountPublication;
            }
        }

        //
        public double Rating
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < _list.Length; i++)
                {
                    sum += _list[i] != null ? _list[i].Rating : 0;
                }

                return sum / _list.Length;
            }
        }

        /// <summary>
        /// Добавлення статей в журнал
        /// </summary>
        /// <param name="list"> Статті які добавляємо в журнал</param>
        public void AddArticles(params Article[] list)
        {
            Array.Resize(ref _list, _list.Length + list.Length);
            for (int i = 0; i < list.Length; i++)
            {
                _list[i + _list.Length - list.Length] = list[i];
            }
        }


        /// <summary>
        /// Добавлення редакторів в журнал
        /// </summary>
        /// <param name="list"></param>
        public void AddEditors(params Person[] list)
        {
            _redactorsList.AddRange(list);
        }

        public override string ToString()
        {
            
            var sb = new StringBuilder();
            sb.Append("namePublication: ");
            sb.Append(Name);
            sb.Append("\n");

            sb.Append("new Frequency(): ");
            sb.Append(_periodicity);
            sb.Append("\n");

            sb.Append("new DateTime()");
            sb.Append(_dateRelease);
            sb.Append("\n");

            sb.Append("countPublication: ");
            sb.Append(_countPublication);
            sb.Append("\n");


            sb.Append("Redactor:");
            foreach (Person p in _redactorsList)
            {
                sb.Append(p);
                sb.Append("\n");
            }


            sb.Append("Articles:");
            foreach (Article a in List)
            {
                sb.Append(a);
                sb.Append("\n");
            }

            return sb.ToString();
        }


        public string ToShortString()
        {
            var sb = new StringBuilder();
            sb.Append("nameArticle: ");
            sb.Append(Name);
            sb.Append("\n");

            sb.Append("new Frequency(): ");
            sb.Append(_periodicity);
            sb.Append("\n");

            sb.Append("new DateTime()");
            sb.Append(_dateRelease);
            sb.Append("\n");

            sb.Append("countPublication: ");
            sb.Append(_countPublication);
            sb.Append("\n");

            sb.Append("new Article[]{}");
            sb.Append(Rating);
            sb.Append("\n");

            

            return sb.ToString();
        }
        
        //laba5
        /// <summary>
        /// створення копії об,єкту з використання серіалізації
        /// </summary>
        /// <param name="obj">оригінал об,єкту</param>
        /// <typeparam name="T">Т - магазин</typeparam>
        /// <returns> копія obj</returns>
        /// <exception cref="ArgumentException">якщо передано об,єкт, який не можна буде сконвертувати</exception>
        public static T DeepCopy<T>(object obj) where T : class
        {
            if (obj is T serialisedObject)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                    try
                    {
                        serializer.WriteObject(ms, serialisedObject);
                        ms.Position = 0;
                        return serializer.ReadObject(ms) as T;
                    }
                    catch (InvalidDataContractException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (SerializationException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        ms.Close();
                    }
                }
            }
            throw new ArgumentException($"I cannot convert { nameof(serialisedObject) } to Magazine");
        }
        
        /// <summary>
        /// для відновлення об,єкту з файлу за допомогою десеріалізації
        /// </summary>
        /// <param name="filePath">шлях до файлу</param>
        /// <param name="magazine">ініціалізує об,єкт</param>
        /// <returns>true - проініціалізовано, false - ні</returns>
         public static bool Load(string filePath, Magazine magazine)
        {
            try
            {
                using (FileStream fstream = File.OpenRead( filePath))
                {
                    byte[] array = new byte[fstream.Length];
                    fstream.Read(array, 0, array.Length);
                    string json = Encoding.Default.GetString(array);

                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                    Magazine deserializedTeam = new Magazine();
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedTeam.GetType());
                    deserializedTeam = ser.ReadObject(ms) as Magazine;

                    magazine.Name = deserializedTeam.Name;
                    magazine.Periodicity = deserializedTeam.Periodicity;
                    magazine.CountPublication = deserializedTeam.CountPublication;
                    magazine.List = deserializedTeam.List;
                    magazine.RedactorsList = deserializedTeam.RedactorsList;
                    magazine. _articleList = deserializedTeam._articleList;

                    ms.Close();
                    fstream.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return false;
        }

        
        /// <summary>
        /// для збереження в файлі за допомогою серіалізації
        /// </summary>
        /// <param name="filePath">шлях до файлу</param>
        /// <param name="saveMagazine"></param>
        /// <returns>True - якщо файл збережено</returns>
        public static bool Save(string filePath, Magazine saveMagazine)
        {
  
            MemoryStream ms = new MemoryStream();
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Magazine));
                ser.WriteObject(ms, saveMagazine);
                byte[] json = ms.ToArray();

                var objectToJson = Encoding.UTF8.GetString(json, 0, json.Length);
                FileStream fstream = new FileStream(filePath, FileMode.OpenOrCreate);
                fstream.SetLength(0);
                byte[] array = Encoding.Default.GetBytes(objectToJson);
                fstream.Write(array, 0, array.Length);
                fstream.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                ms.Close();
            }
            return false;
        }
        
        /// <summary>
        /// для збереження даних об,єкту в файлі за допомогою серіалізації
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
         public bool Save(string filePath)
        {
            
            return Magazine.Save(filePath, this);
        }

        /// <summary>
        /// для ініціалізації об,єкту даними з файлу за допомогою десеріалізації
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool Load(string filePath)
        {
          return  Magazine.Load(filePath, this);
           
        }

        public bool AddArticleFromConsole()
        {
            Console.WriteLine("Введiть данi для об'єкту Article наступного формату: " +
                              "назва статті;" +
                              "рейтинг статті;" +
                              "Автор: " +
                              "iм'я;прiзвище;" +
                              "дата народження(формат: YYYY:MM:DD)\n" +
                              "Приклад:  Auto;3;James;Bay;1990:04:23");

            Person person = new Person();
            Article article = new Article();
            var input = Console.ReadLine();
            string[] splitedString = new string[] { "" };

            if (input != null)
            {
                splitedString = input.Split(';');
            }

            try
            {
                article.Name = splitedString[0];

                article.Rating = double.Parse(splitedString[1]);
                
                person.Name = splitedString[2];
                
                person.Surname = splitedString[3];
                
                var yearOfBirth = int.Parse(splitedString[4].Split(':')[0]);
                var monthOfBirth = int.Parse(splitedString[4].Split(':')[1]);
                var dayOfBirth = int.Parse(splitedString[4].Split(':')[2]);
                person.DateBirth = new DateTime(yearOfBirth, monthOfBirth, dayOfBirth);
                article.Author = person;
                AddArticles(article);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return false;
        }
        
        
    }
}