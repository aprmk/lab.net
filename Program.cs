﻿using System;
using System.IO;

namespace laba.NET
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Edition e1 = new Edition();
            Edition e2 = new Edition();


            try
            {
                e1.CountPublication = -9;
            }
            catch
            {
                Console.WriteLine("Некоректне значення тиражу");
            }

            Console.WriteLine($"{e1.GetHashCode()}, {e2.GetHashCode()}");
            if (e1 == e2)
            {
                Console.WriteLine("Об'єкти співпіли");
            }
            else
            {
                Console.WriteLine("Об'єкти не співпали");
            }

            if (e1.GetHashCode() == e2.GetHashCode())
            {
                Console.WriteLine("Співпали хеші");
            }
            else
            {
                Console.WriteLine("Не співпали хеші");
            }


            Magazine m = new Magazine();

            //Значення властивості типу Edition для об,єкта Magazine
            Console.WriteLine("Значення властивості типу Edition для об,єкта Magazine");
            Console.WriteLine(m.obj.ToString());

            //Створення копії об,єкта Magazine - DeepCopy
            Magazine m2 = (Magazine) m.DeepCopy();
            m.Name = "Norway";
            m.DateRelease = DateTime.Today;
            m.CountPublication = 1;


            Console.WriteLine();
            Console.WriteLine("Копiя");
            Console.WriteLine(m2.ToString());
            Console.WriteLine();
            Console.WriteLine("Оригінал");
            Console.WriteLine(m.ToString());
            Console.WriteLine();

            //Добавлення елементів в список статей та редакторів журналу
            Article article = new Article();
            Person redactor = new Person();
            m.AddArticles(article);
            m.AddEditors(redactor);
            Console.WriteLine("Добавлення елементів в список статей та редакторів журналу");
            Console.WriteLine(m.ToString());

            //вивід статей з рейтингом >1 
            Console.WriteLine("вивід статей з рейтингом >1 ");
            foreach (var art in m[1])
            {
                Console.WriteLine(art.ToString());
            }

            //Вивести список статей у який міститься слово - SAVE
            Console.WriteLine("список статей у який міститься слово - SAVE");
            foreach (var art in m["SAVE"])
            {
                Console.WriteLine(art.ToString());
            }

            //сортування  по назві, даті, тираж - з інтерфейсом IComparer що реалізовано в класі Edition
            MagazineCollection mg = new MagazineCollection();
            Magazine test = new Magazine();

            test.DateRelease = test.DateRelease.AddDays(-2);
            test.Name = "Test";
            test.CountPublication = 6;
            test.AddArticles(article);


            mg.AddDefaults();
            mg.AddMagazines(test, m);

            Console.WriteLine("сортування по даті видання з використанням IComparer БЕЗ");
            Console.WriteLine(mg.ToString());

            mg.SortName();

            Console.WriteLine("сортування по НАЗВІ видання з використанням IComparer ");
            Console.WriteLine(mg.ToString());

            mg.SortDate();

            Console.WriteLine("сортування по ДАТІ видання з використанням IComparer");
            Console.WriteLine(mg.ToString());

            mg.SortCountPublication();

            Console.WriteLine("сортування по ТИРАЖУ видання з використанням IComparer");
            Console.WriteLine(mg.ToString());

            //список магазинів з середнім рейтингом >= value
            var list1 = mg.RatingGroup1(3);
            Console.WriteLine("список магазинів з середнім рейтингом >= value");

            for (int i = 0; i < list1.Count; i++)
            {
                Console.WriteLine(list1[i]);

            }

            Console.WriteLine();

            var list2 = mg.RatingGroup2(3);

            Console.WriteLine("список ПОГРУПОВАНИХ магазинів з середнім рейтингом >= value");
            for (int i = 0; i < list2.Count; i++)
            {
                Console.WriteLine($"Group key={list2[i].Key}");
                Console.WriteLine("Items=");
                foreach (var mag in list2[i])
                {
                    Console.WriteLine(mag.ToString());
                }
            }

            Console.WriteLine();

            // максимальне значення середнього рейтингу статей
            Console.WriteLine("максимальне значення середнього рейтингу статей");
            Console.WriteLine(mg.MaxValueSerdn.ToString());
            Console.WriteLine();

            // періодичність виходу журналу 
            Console.WriteLine("періодичність виходу журналу");
            var list3 = mg.PeriodicityDateMagazines;
            foreach (var obj in list3)
            {
                Console.WriteLine(obj.ToString());

            }

            Console.WriteLine();

            //Пошук в колекціях першого, центрального, останнього, та елементу що не входить
            /* Console.WriteLine("Пошук в колекціях першого, центрального, останнього, та елементу що не входить");
            TestCollections tc = new TestCollections((int)1e7);
            tc.TimeSearch(); */


            //Laba4

            MagazineCollection mg1 = new MagazineCollection();
            MagazineCollection mg2 = new MagazineCollection();

            Listener l1 = new Listener();
            Listener l2 = new Listener();

            // добавлення обробника на події MagazineAdded і MagazineAReplace з перщої колекціїї
            mg1.MagazineAdded += l1.MagazineAdded;
            mg1.MagazineReplaced += l1.MagazineAReplace;

            mg2.MagazineAdded += l1.MagazineAdded;
            mg2.MagazineReplaced += l1.MagazineAReplace;

            // добавлення обробника на події MagazineAdded і MagazineAReplace з другої колекціїї
            mg2.MagazineAdded += l2.MagazineAdded;
            mg2.MagazineReplaced += l2.MagazineAReplace;


            Magazine add = new Magazine();

            add.DateRelease = test.DateRelease.AddDays(1);
            add.Name = "Add";
            add.CountPublication = 4;
            add.AddArticles(article);

            Magazine replace1 = new Magazine();

            replace1.DateRelease = test.DateRelease.AddDays(1);
            replace1.Name = "Replace1";
            replace1.CountPublication = 2;
            replace1.AddArticles(article);

            Magazine replace2 = new Magazine();

            replace2.DateRelease = test.DateRelease.AddDays(1);
            replace2.Name = "Replace2";
            replace2.CountPublication = 2;
            replace2.AddArticles(article);

            //зміни для першої колекції
            mg1.AddMagazines(add);
            mg1.AddDefaults();

            mg1.Replace(2, replace1);
            mg1[1] = replace2;

            //зміни для другої колекції
            mg2.AddMagazines(add);
            mg2.AddDefaults();

            mg2.Replace(2, replace1);
            mg2[1] = replace2;

            Console.WriteLine("зміни для першої і другої  колекції");
            Console.WriteLine(l1.ToString());
            Console.WriteLine("зміни для другої колекції");
            Console.WriteLine(l2.ToString());


            //laba 5
            Console.ForegroundColor = ConsoleColor.Green;

            // 1) Створення копії об,єкту за допомогою серіалізації
            Magazine copy = Magazine.DeepCopy<Magazine>(replace1);
            Console.WriteLine("Створення копії об,єкту за допомогою серіалізації");
            Console.WriteLine();
            Console.WriteLine("Оригінал об,єкту");
            Console.WriteLine(replace1);
            Console.WriteLine("Копія об,єкту");
            Console.WriteLine(copy);

            // 2) Введення ім,я файлу

            Console.WriteLine("Введiть iм,я файлу:");

            string nameFile = Console.ReadLine();
            string filePath = Path.Combine($"{nameFile}.txt");
            Magazine loaded = new Magazine();
            if (File.Exists(filePath))
            {
                loaded.Load(filePath);
            }
            else
            {
                Console.WriteLine("Файлу немає, тому створимо його");
                File.Create(filePath);
            }
            Console.WriteLine(loaded);
            Console.WriteLine();
            
            // 4) для Т викликаємо AddArticleFromConsole і Save
            loaded.AddArticleFromConsole();
            loaded.Save(filePath);
            Console.WriteLine("Вивід об,єкту Т");
            Console.WriteLine(loaded);
            
            // 5) для Т викликаємо Load, AddArticleFromConsole, Save
            Magazine.Load(filePath, loaded);
            loaded.AddArticleFromConsole();
            Magazine.Save(filePath, loaded);
            
            Console.WriteLine();
            Console.WriteLine("Вивід об,єкту Т");
            Console.WriteLine(loaded);
            Console.ForegroundColor = ConsoleColor.White;

            



            /* m.ToShortString();
            Console.WriteLine(m.ToShortString());
            Console.WriteLine(m[Frequency.Weekly]);
            Console.WriteLine(m[Frequency.Montly]);
            Console.WriteLine(m[Frequency.Yearly]);


            m.Name = "The Garage";
            m.DateRelease = DateTime.Now;
            m.Periodicity = Frequency.Montly;
            m.CountPublication = 3;
            Console.WriteLine(m.ToShortString());
            Article a = new Article();
            Article a2 = new Article();
            m.AddArticles(a, a2, null);

            Console.WriteLine(m.ToString());

            string str = Console.ReadLine();
            string[] words = new string[2];
            if (str != null) words = str.Split(' ', ',');
            var nRows = Convert.ToInt32(words[0]);
            var nColumns = Convert.ToInt32(words[1]);
            int nCount = nRows * nColumns;
            Console.WriteLine(string.Format("nRows: {0}  nColumns: {1}", nRows, nColumns));


            Magazine[] oneDimArray = new Magazine[nCount];
            for (int i = 0; i < nCount; i++)
            {
                oneDimArray[i] = new Magazine();
                oneDimArray[i].AddArticles(a, a, a, a, a);
            }


            Magazine[,] twoDimArray = new Magazine[nRows, nColumns];
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    twoDimArray[i, j] = new Magazine();
                    twoDimArray[i, j].AddArticles(a, a, a, a, a);
                }
            }

            //TODO Ступінчастий масив 
            Magazine[][] steppedArray = new Magazine[nRows][];
            for (int i = 0; i < nRows; i++)
            {
                steppedArray[i] = new Magazine[nColumns];
            }

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    steppedArray[i][j] = new Magazine();
                    steppedArray[i][j].AddArticles(a, a, a, a, a);
                }
            }

            int sum = 0, countt = 0;
            while (sum <= nCount)
            {
                sum += countt++;
            }

            Magazine[][] steppedArrayDifferent = new Magazine[countt--][];
            for (int i = 0, count = 0; count < countt; i++)
            {
                steppedArrayDifferent[i] = new Magazine[count + 1];
                for (int j = 0; j < count; j++)
                {
                    steppedArrayDifferent[i][j] = new Magazine();
                    steppedArrayDifferent[i][j].AddArticles(a, a, a, a, a);
                }

                count++;
            }

            //TODO Тестування по часу
            double tmp = 0;
            Console.WriteLine("OneArrayTest");
            var start = Environment.TickCount;
            for (int i = 0; i < nCount; i++)
            {
                tmp = oneDimArray[i].Rating;
            }

            var finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            Console.WriteLine("TwoArrayTest");
            start = Environment.TickCount;
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    tmp = twoDimArray[i, j].Rating;
                }
            }

            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            Console.WriteLine("SteppedArrayTest");
            start = Environment.TickCount;
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    tmp = steppedArray[i][j].Rating;
                }
            }

            finish = Environment.TickCount;
            Console.WriteLine(finish - start);

            Console.WriteLine("SteppedArrayDifferentTest");
            start = Environment.TickCount;
            for (int i = 0, count = 0; count < countt; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    tmp = steppedArrayDifferent[i][j].Rating;
                }

                count++;
            }

            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
        } */
        }

    }
}