﻿using System;
using System.Security.Principal;
using System.Text;

namespace laba.NET
{
    public class Article : IRateAndCopy
    {
        public Person Author { get; set; }
        public string Name { get; set; }
        public double Rating { get; set; }

        public Article(Person author, string name, double ratingArticle)
        {
            Author = author;
            Name = name;
            Rating = ratingArticle;
        }
        
        public Article ShallowCopy()
        {
            return (Article)this.MemberwiseClone();
        }
        
        public virtual object DeepCopy()
        {
            Article a = new Article(Author.DeepCopy(), Name, Rating);
            return a;
        }

        public Article() : this(new Person(), "<SAVE25>", 4)
        {
        }

        public override string ToString()
        {
            return 
                $"authorArticle {Author} {Environment.NewLine}nameArticle {Name} {Environment.NewLine}ratingArticle {Rating} {Environment.NewLine}";
            
        }
    }
}