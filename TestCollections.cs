using System;
using System.Collections;
using System.Collections.Generic;

namespace laba.NET
{
    public class TestCollections//:IDictionary<Edition, Magazine>
    {
        private System.Collections.Generic.List<Edition> _editions;
        private System.Collections.Generic.List<string> _list;
        private System.Collections.Generic.Dictionary<Edition, Magazine> _magazinesByEdition;
        private System.Collections.Generic.Dictionary<string, Magazine> _magazinesByString;   

        
        public TestCollections(int n)
        {
            _editions = new List<Edition>();
            _list = new List<string>();
            _magazinesByEdition = new Dictionary<Edition, Magazine>();
            _magazinesByString = new Dictionary<string, Magazine>();

            for (int i = 0; i < n; i++)
            {
                Magazine m = CreateCollections(i);
                _editions.Add(m.obj);
                _list.Add(m.Name);
                _magazinesByEdition.Add(m.obj, m);
                _magazinesByString.Add(m.Name, m);
            }
        }

        public void TimeSearch()
        {
            
            string firstString = _list[0];
            Edition firstIntID = _editions[0];
            
            Console.WriteLine("Time Search Dictionary FIRST String");
            var start = Environment.TickCount;
            _magazinesByString.TryGetValue(firstString, out Magazine mFirst1);
            var finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search Dictionary FIRST Edition");
            start = Environment.TickCount;
            _magazinesByEdition.TryGetValue(firstIntID, out Magazine mFirst2);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List FIRST String");
            start = Environment.TickCount;
            string firstFound2 = _list.Find(item=>item==firstString);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List FIRST Edition");
            start = Environment.TickCount;
            Edition firstFound1 = _editions.Find(item=>item==firstIntID);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            
            string centerString = _list[_list.Count/2];
            Edition centerIntID = _editions[_editions.Count/2];
            
            
            Console.WriteLine("Time Search Dictionary CENTER String");
            start = Environment.TickCount;
            _magazinesByString.TryGetValue(centerString, out Magazine mCenter1);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search Dictionary CENTER Edition");
            start = Environment.TickCount;
             _magazinesByEdition.TryGetValue(centerIntID, out Magazine mCenter2);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List CENTER String");
            start = Environment.TickCount;
            string centerFound2 = _list.Find(item=>item==centerString);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List CENTER Edition");
            start = Environment.TickCount;
            Edition centerFound1 = _editions.Find(item=>item==centerIntID);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            
            string endString = _list[_list.Count-1];
            Edition endIntID = _editions[_editions.Count-1];
            
            
            double tmp = 0;
            Console.WriteLine("Time Search Dictionary String");
            start = Environment.TickCount;
             _magazinesByString.TryGetValue(endString, out Magazine mEnd1);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search Dictionary Edition");
            start = Environment.TickCount; 
            _magazinesByEdition.TryGetValue(endIntID, out Magazine mEnd2);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List String");
            start = Environment.TickCount;
            string endFound2 = _list.Find(item=>item==endString);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List Edition");
            start = Environment.TickCount;
            Edition endFound1 = _editions.Find(item=>item==endIntID);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            string notCollectionsString = "Динамо";
            Edition notCollectionsIntID = new Edition("Динамо", DateTime.Now, 1);
            
            Console.WriteLine("Time Search Dictionary FIRST String");
            start = Environment.TickCount;
             _magazinesByString.TryGetValue(notCollectionsString, out Magazine mNot1);//[notCollectionsString];
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search Dictionary FIRST Edition");
            start = Environment.TickCount;
            _magazinesByEdition.TryGetValue(notCollectionsIntID, out Magazine mNot2);//[notCollectionsIntID];
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List FIRST String");
            start = Environment.TickCount;
            string notCollectionsFound2 = _list.Find(item=>item==notCollectionsString);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            Console.WriteLine("Time Search List FIRST Edition");
            start = Environment.TickCount;
            Edition notCollectionsFound1 = _editions.Find(item=>item==notCollectionsIntID);
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
           
        }
        
      public static Magazine CreateCollections(int n)
        {
            var magazine = new Magazine("Реал"+n, Frequency.Weekly, DateTime.Now, 7+n, new ArrayList(), new ArrayList());
            return magazine;
        }
    }
}