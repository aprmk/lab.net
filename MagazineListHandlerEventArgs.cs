using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;

namespace laba.NET
{
    public delegate void MagazineListHandler(object source, MagazineListHandlerEventArgs args);
        
    public class MagazineListHandlerEventArgs : System.EventArgs
    {
    
    public string NameCollectionsEvent{ 
        get;
        set;
    }

    public string TypeChangeCollections
    {
        get;
        set;
    }

    public int NumberElementChanges
    {
        get;
        set;
    }
    
    public MagazineListHandlerEventArgs(string nameCollectionsEvent, string typeChangeCollections, int numberElementChanges) : base()
    {
        NameCollectionsEvent = nameCollectionsEvent;
        TypeChangeCollections = typeChangeCollections;
        NumberElementChanges = numberElementChanges;
   
    }

    public override string ToString()
    {
        return 
            $"NameCollectionsEvent {NameCollectionsEvent} {Environment.NewLine}TypeChangeCollections {TypeChangeCollections} {Environment.NewLine}NumberElementChanges {NumberElementChanges} {Environment.NewLine}";
       
    }

    }
}