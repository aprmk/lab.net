using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata;
using System.Text;
using System.Text.RegularExpressions;

namespace laba.NET
{
    public class MagazineCollection 
    {
        public string NameCollections
        {
            get;
            set;
        }
        
        private System.Collections.Generic.List<Magazine> _magazine;

        /// <summary>
        /// заміна елемента з номером j зі списку List<Magazine> на елемент _magazine
        /// </summary>
        /// <param name="j"></param>
        /// <param name="magazine"></param>
        /// <returns></returns>
        public bool Replace(int j, Magazine magazine)
        {
            if (j >= _magazine.Count || j < 0)
            {
                return false;
            } else
            {
                _magazine[j] = magazine;
               
                MagazineListHandlerEventArgs mag = new MagazineListHandlerEventArgs(NameCollections, "Замінили елемент", j );
                if(MagazineReplaced !=null) MagazineReplaced(this, mag);
               
                return true;
            }
        }

        /// <summary>
        /// індексатор, для доступу до елементу списку
        /// </summary>
        /// <param name="index"></param>
        public Magazine this[int index]
        {
            get { return _magazine[index]; }
            set
            {
                _magazine[index] = value;
                
                MagazineListHandlerEventArgs mag = new MagazineListHandlerEventArgs(NameCollections, "Замінили елемент", index );
                if(MagazineReplaced !=null) MagazineReplaced(this, mag);

            }
        }
        
        

        /// <summary>
        /// максимальне значення середнього рейтингу статей
        /// </summary>
        public double MaxValueSerdn
        {
            get
            {
                double maxValueSerdn = _magazine.Max(SerednRaitingMagaz);
                return maxValueSerdn;
            }
        }

        /// <summary>
        /// періодичність виходу журналу 
        /// </summary>
        public IEnumerable<Magazine> PeriodicityDateMagazines
        {
            get
            {
                IEnumerable<Magazine> evens = from i in _magazine
                    where i.Periodicity == Frequency.Montly 
                    select i;
                return evens;
            } 
        }

        /// <summary>
        /// список магазинів з середнім рейтингом >= value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<Magazine> RatingGroup1(double value)
        {
            IEnumerable<Magazine > evens = from i in _magazine
                where i.Rating >= value
                select i;
            return evens.ToList();
        }
       
        /// <summary>
        /// список ПОГРУПОВАНИХ магазинів з середнім рейтингом >= value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<IGrouping<double,Magazine >> RatingGroup2(double value)
        {
            IEnumerable<IGrouping<double,Magazine > > evens = from i in _magazine
                where i.Rating >= value
                group i by i.Rating;
            // select i;
            return evens.ToList();
        }
        /// <summary>
        /// функція що бере середній рейтинг магазину
        /// <param name="magszine">магазин, рейтинг якого беремо</para>
        /// </summary>
        /// <returns>Середній рейтинг цього магазину</returns>
        public double SerednRaitingMagaz(Magazine _magazine)
        {
            return _magazine.Rating;
        }



        /// <summary>
        /// конструктор без параметрів для MagazineCollection
        /// </summary>
        public MagazineCollection()
        {
            _magazine = new List<Magazine>();
        }

        /// <summary>
        /// додавання елементів типу Magazine в список List<Magazine>
        /// </summary>
        public void AddDefaults()
        {
            List<Magazine> magazines = new List<Magazine>(){
                new Magazine("Volvo", Frequency.Montly, DateTime.Now, 2, new ArrayList(), new ArrayList()),
                new Magazine(),
            };
            this._magazine.AddRange(magazines);
            for (int i = _magazine.Count - magazines.Count; i < _magazine.Count; i++)
            {
                MagazineListHandlerEventArgs mag = new MagazineListHandlerEventArgs(NameCollections, "Added", i);
                MagazineAdded?.Invoke(this, mag);
            }
        }
       
        /// <summary>
        /// додавання елементів в список List<Magazine>
        /// </summary>
        /// <param name="magazine"></param>
        public void AddMagazines(params Magazine[] magazine)
        {
            _magazine.AddRange(magazine);
            for (int i = _magazine.Count - magazine.Length; i < _magazine.Count; i++)
            {
                MagazineListHandlerEventArgs mag = new MagazineListHandlerEventArgs(NameCollections, "Added", i);
                MagazineAdded?.Invoke(this, mag);
            }
        }
      
       
        /// <summary>
        /// метод ToString для списку List<Magazine>
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            
            var sb = new StringBuilder();
           
            sb.Append("Magazine:");
            foreach (Magazine a in _magazine)
            {
                sb.Append(a);
                sb.Append("\n");
            }

            return sb.ToString();
        }
       
        /// <summary>
        /// метод ToShortString для списку List<Magazine> без статей і редакторів
        /// </summary>
        /// <returns></returns>
        public string ToShortString()
        {
            var sb = new StringBuilder();
           
            sb.Append("Magazine:");
            foreach (Magazine a in _magazine)
            {
                sb.Append(a.ToShortString());
                sb.Append("\n");
            }

            return sb.ToString();
        }
       
        
        /// <summary>
        /// сортування по НАЗВІ з інтерфейсом IComparer що реалізовано в класі Edition
        /// </summary>
        public void SortName()
        {
            _magazine.Sort();
        }
       
        /// <summary>
        /// сортування по ДАТІ з інтерфейсом IComparer що реалізовано в класі Edition
        /// </summary>
        public void SortDate()
        {
            Edition sort = new Edition(); 
            _magazine.Sort(sort);
        }
        class CountPublicationsComparator: IComparer<Edition>
        {
            public int Compare(Edition x, Edition y)
            {
                return x.CountPublication.CompareTo(y.CountPublication);
            }
        }
        /// <summary>
        /// сортування по ТИРАЖУ з інтерфейсом IComparer що реалізовано в класі Edition
        /// </summary> 
        public void SortCountPublication()
        {
            _magazine.Sort(new CountPublicationsComparator());
        }

        public event MagazineListHandler MagazineAdded;
        public event MagazineListHandler MagazineReplaced;
        
        
    }
    
}