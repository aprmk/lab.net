﻿using System;
using System.Text;

namespace laba.NET
{
    public class Person
    {
        private string _name;
        private string _surname;
        private DateTime _dateBirth;

        public Person(string name, string surname, DateTime dateBirth)
        {
            Name = name;
            Surname = surname;
            DateBirth = dateBirth;
            DateBirth = dateBirth.ToLocalTime();
        }

        public Person() : this("Nastya", "Boichuk", new DateTime(2000, 2, 4))
        {
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        public DateTime DateBirth
        {
            get { return _dateBirth; }
            set { _dateBirth = value; }
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }


        public int YearBirth
        {
            get { return _dateBirth.Year; }
            set { _dateBirth = new DateTime(value, _dateBirth.Month, _dateBirth.Day); }
        }

        public Person ShallowCopy()
        {
            return (Person)this.MemberwiseClone();
        }
        
        public virtual Person DeepCopy()
        {
            Person p = new Person(Name, Surname, DateBirth);
            return p;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType()))
            {
                return false;
            }

            Person p = (Person) obj;
            return Name == p.Name && Surname == p.Surname && DateBirth.Equals(p.DateBirth);
        }

        public static bool operator ==(Person obj1, Person obj2)
        {
            return obj1 != null && obj1.Equals(obj2);
        }

        public static bool operator !=(Person obj1, Person obj2)
        {
            return obj1 != null && !obj1.Equals(obj2);
        }

        public override string ToString()
        {
            return 
            $"name {Name} {Environment.NewLine} surname {Surname} {Environment.NewLine}date_birth {DateBirth.ToLongDateString()} {Environment.NewLine}";

        }

        public string ToShortString()
        {

            return
                $"name {Name} {Environment.NewLine}surname {Surname} {Environment.NewLine}";
        }
    }
}